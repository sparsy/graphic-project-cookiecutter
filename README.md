# Graphic Project Cookiecutter #

Simple Cookiecutter for basic graphic project.

This is the current structure it create:

- `references` folder to put your references files in
- `images` is where you put your images to be used in your project
- `export` is where you save your completed work ready to be published
  the content of this folder is not tracked
    